﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading;

namespace LINQ
{
    internal class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */

        private static void Main()
        {
           // PrintOutOfStock();
            InStockMoreThan3();
            //customersInWashington();
            //justProductNames();
            //unitPriceBy25();
            //upperProductNames();
            //evenNumberUnits();
            //productNameCategoryUnitPriceRename();
            //bLessThanC();
            //lessThan500();
            //firstThreeElements();
            //first3OrdersOfWA();
            //skipFirst3();
            //exceptFirstTwo();
            //elementGreaterThan6();
            // lessThanPosition();
            //divisibleByThree();
            //nameByAlphabet();
            //descendingUnitsStock();
            //listByCategoryUnitP();
            //reverseC();
            //remainderBy5();
            //byCategory();
            //byYearMonth();
            //Unique();
            // uniqueAandB();
            //sharedAandB();
            //notAandB();
            //only12();
            //contains789();
            //outofStockCat();
            //lessThan9();
            //CatWithPinStock();
            //onlyOdds();
            //customerIDCount();
            //productCount();

            //totalUnitbyCat();
            //lowestP();
            //highestP();

           // averageP();







            Console.ReadLine();
        }

        //1. Find all products that are out of stock.



        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            ////var results = products.Where(p => p.UnitsInStock == 0);
            //var results = from p in products
            //              where p.UnitsInStock == 0
            //              select p;

            //foreach (var product in results)
            //{
            //    Console.WriteLine(product.ProductName);
            //}

            var result = from p in products
                         where p.UnitsInStock < 1
                         select p;

            foreach (var item in result)
            {
                Console.WriteLine("Product Name- {0}    Units in Stock- {1}", item.ProductName, item.UnitsInStock);
            }


        }

        //2. Find all products that are in stock and cost more than 3.00 per unit.
        private static void InStockMoreThan3()
        {
            var products = DataLoader.LoadProducts();

            ////var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);
            //var results = from p in products
            //    where p.UnitsInStock > 0 && p.UnitPrice > 3
            //    select p;

            //foreach (var product in results)
            //{
            //    Console.WriteLine("{0} has {1} in stock with unit price {2}", product.ProductName,
            //        product.UnitsInStock, product.UnitPrice);
            //}

            var result = from p in products
                         where (p.UnitsInStock > 0) && p.UnitPrice > 3
                         select p;

            foreach (var item in result)
            {
                Console.WriteLine("Product Name- {0}  Units In Stock- {1} Cost- {2}", item.ProductName, item.UnitsInStock, item.UnitPrice);
            }

            //var result = from p in products
            //    where p.UnitsInStock > 0 && p.UnitPrice > 3
            //    select p;

            //var result1 = result.GroupBy(n => n.Category);

            //foreach (var item in result1)
            //{
            //    Console.WriteLine("\t \t \t \t Category -{0}", item.Key );
            //    foreach (var n in result)
            //    {
            //        Console.WriteLine("Name-{0}   InStock-{1}   Price-{2}", n.ProductName, n.UnitsInStock, n.UnitPrice);
            //    }
            //}


        }

        // 3. Find all customers in Washington, print their name then their orders. (Region == "WA")

        private static void customersInWashington()
        {
            var customers = DataLoader.LoadCustomers();

            var result = from c in customers
                where c.Region == "WA"
                select c;

            foreach (var customer in result)
            {
                Console.WriteLine(" {0} order is {1} in region {2}", customer.CompanyName, customer.Orders,
                    customer.Region);
            }



        }

        //4. Create a new sequence with just the names of the products.

        public static void justProductNames()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                select p.ProductName;

            foreach (var product in results)
            {
                Console.WriteLine("{0} is the product name", product);
            }

        }

        // 5. Create a new sequence of products and unit prices where the unit prices are increased by 25%.

        public static void unitPriceBy25()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                select p;


            foreach (var product in results)
            {
                Console.WriteLine("{0} increased by 25% is {1}", product.ProductName,
                    product.UnitPrice + product.UnitPrice*25/100);
            }
        }



        //6. Create a new sequence of just product names in all upper case.

        public static void upperProductNames()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                select p.ProductName;

            foreach (var product in results)
            {
                Console.WriteLine("{0} is with uppercase", product.ToUpper());
            }
        }


        //7. Create a new sequence with products with even numbers of units in stock.

        public static void evenNumberUnits()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                where p.UnitsInStock%2 == 0
                select p;

            foreach (var product in results)
            {
                Console.WriteLine(" {0} is even unit stocked", product.UnitsInStock);
            }

        }

        // 8. Create a new sequence of products with ProductName, Category, and rename UnitPrice to Price.

        public static void productNameCategoryUnitPriceRename()
        {
            var products = DataLoader.LoadProducts();


            var results = from p in products
                select p;


            foreach (var product in results)
            {
                decimal Price = product.UnitPrice;


                Console.WriteLine("{0} is in category {1} with price {2}", product.ProductName, product.Category, Price);
            }

        }


        // 9. Make a query that returns all pairs of numbers from both arrays such that the number from numbersB is less than the number from numbersC.

        public static void bLessThanC()
        {
            var numbersB = DataLoader.NumbersB;
            var numbersC = DataLoader.NumbersC;

            //var result = from b in numbersB
            //    from c in numbersC
            //    where b < c
            //    select new {b, c};

            var result = numbersB.SelectMany(b => numbersC, (b, c) => new {b, c}).Where(x => x.b < x.c);

            foreach (var element in result)
            {
                Console.WriteLine(element);
            }



        }

        //10. Select CustomerID, OrderID, and Total where the order total is less than 500.00.


        public static void lessThan500()
        {
            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                from o in c.Orders
                where o.Total < 500
                select new
                {
                    customerID = c.CustomerID,
                    orderID = o.OrderID,
                    total = o.Total

                };


            foreach (var customer in results)
            {

                Console.WriteLine("Customer ID {0} with order id {1} with total {2}. ", customer.customerID,
                    customer.orderID, customer.total);
            }



        }


        // 11. Write a query to take only the first 3 elements from NumbersA.Review

        public static void firstThreeElements()
        {
            int[] _numbers = DataLoader.NumbersA;

            var results = _numbers.Take(3);

            foreach (var i in results)
            {
                Console.WriteLine(i);
            }


        }

        //12. Get only the first 3 orders from customers in Washington.

        public static void first3OrdersOfWA()
        {

            var customers = DataLoader.LoadCustomers();

            var wasOrders = from c in customers
                from o in c.Orders
                where c.Region == "WA"
                select o;

            var results = wasOrders.Take(3);


            foreach (var customer in  results)
            {
                Console.WriteLine("{0} ", customer.OrderDate);
            }


        }


        // 13. Skip the first 3 elements of NumbersA.

        public static void skipFirst3()
        {
            var numbers = DataLoader.NumbersA;

            var results = numbers.Skip(3);

            foreach (var number in results)
            {
                Console.WriteLine(number);
            }


        }

        //14. Get all except the first two orders from customers in Washington.

        public static void exceptFirstTwo()
        {
            var customers = DataLoader.LoadCustomers();

            var allOrders = from c in customers
                from o in c.Orders
                where c.Region == "WA"
                select o;

            var result = allOrders.Skip(2);


            foreach (var butTwo in result)
            {
                Console.WriteLine(butTwo.OrderDate);
            }



        }



        // 15. Get all the elements in NumbersC from the beginning until an element is greater or equal to 6.

        public static void elementGreaterThan6()
        {
            var numbers = DataLoader.NumbersC;

            var result = numbers.TakeWhile(i => i < 6);




            foreach (var n in result)
            {
                Console.WriteLine(n);
            }

        }


        // 16. Return elements starting from the beginning of NumbersC until a number is hit that is less than its position in the array.

        public static void lessThanPosition()
        {
            var numbers = DataLoader.NumbersC;



            var result = numbers.TakeWhile(i => Array.IndexOf(numbers, i) < i);


            foreach (var x in result)
            {
                Console.WriteLine(x);
            }
        }


        // 17. Return elements from NumbersC starting from the first element divisible by 3.

        public static void divisibleByThree()
        {
            var numbers = DataLoader.NumbersC;

            var results = numbers.Where(n => n%3 == 0);

            foreach (var x in results)
            {
                Console.WriteLine(x);
            }


        }


        //18. Order products alphabetically by name.

        public static void nameByAlphabet()
        {
            var products = DataLoader.LoadProducts();



            var result1 = products.OrderBy(s => s.ProductName);

            foreach (var x in result1)
            {
                Console.WriteLine(x.ProductName);
            }
        }



        // 19. Order products by UnitsInStock descending.

        public static void descendingUnitsStock()
        {
            var products = DataLoader.LoadProducts();

            var result = from p in products
                select p.UnitsInStock;

            var result1 = result.OrderByDescending(o => o);

            foreach (var x in result1)
            {
                Console.WriteLine(x);
            }

        }

        // 20. Sort the list of products, first by category, and then by unit price, from highest to lowest.

        public static void listByCategoryUnitP()
        {
            var products = DataLoader.LoadProducts();

            //var results = from p in products
            //              orderby p.Category, p.UnitPrice descending
            //              select p;
            var results = products.OrderBy(p => p.Category).ThenByDescending(p => p.UnitPrice);

            foreach (var result in results)
            {
                Console.WriteLine("{0} {1} {2}", result.ProductName, result.Category, result.UnitPrice);
            }
        }

        //21. Reverse NumbersC.

        public static void reverseC()
        {
            var c = DataLoader.NumbersC;

            var result = c.Reverse();

            foreach (var n in result)
            {
                Console.WriteLine(n);
            }
        }


        //22. Display the elements of NumbersC grouped by their remainder when divided by 5.

        public static void remainderBy5()
        {
            var numbers = DataLoader.NumbersC;

            var result = from i in numbers
                group i by i%5;


            foreach (var group in result)
            {
                Console.WriteLine(group.Key);
                foreach (var i in group)
                {
                    Console.WriteLine("\t{0}", i);
                }
            }

        }

        //23. Display products by Category.

        public static void byCategory()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                select p;

            foreach (var c in results)
            {
                Console.WriteLine("{0} is in {1}", c.ProductName, c.Category);
            }
        }

        //24. Group customer orders by year, then by month.

        public static void byYearMonth()
        {
            var customers = DataLoader.LoadCustomers();

            var result = from c in customers
                from o in c.Orders
                select o;

            var result1 = result.GroupBy(g => new {g.OrderDate.Year, g.OrderDate.Month});



            foreach (var order in result1)
            {
                Console.WriteLine(order.Key);
                foreach (var item in order)
                {
                    Console.WriteLine("\t {0}  \t{1} \t{2}", item.OrderDate, item.OrderID, item.Total);
                }
            }

        }


        //25. Create a list of unique product category names.

        public static void Unique()
        {
            var products = DataLoader.LoadProducts();


            var result1 = (products.Select(p => p.Category))
                .Distinct();



            foreach (var name in result1)
            {
                Console.WriteLine(name);
            }

        }

        // 26. Get a list of unique values from NumbersA and NumbersB.

        public static void uniqueAandB()
        {
            var numberA = DataLoader.NumbersA;
            var numberB = DataLoader.NumbersB;


            var result = numberA.Union(numberB);

            foreach (var element in result)
            {
                Console.WriteLine(element);
            }
        }



        // 27. Get a list of the shared values from NumbersA and NumbersB.

        public static void sharedAandB()
        {
            var numberA = DataLoader.NumbersA;
            var numberB = DataLoader.NumbersB;

            var result = numberA.Intersect(numberB);

            foreach (var element in result)
            {
                Console.WriteLine(element);
            }
        }


        // 28. Get a list of values in NumbersA that are not also in NumbersB.

        public static void notAandB()
        {
            var numberA = DataLoader.NumbersA;
            var numberB = DataLoader.NumbersB;

            var result = numberA.Except(numberB);

            foreach (var element in result)
            {
                Console.WriteLine(element);
            }
        }


        //29. Select only the first product with ProductID = 12 (not a list).

        public static void only12()
        {
            var products = DataLoader.LoadProducts();

            var result = from p in products
                where p.ProductID == 12
                select p;

            var result1 = result.Select(p => p.ProductName).Take(1);


            foreach (var id in result1)
            {
                Console.WriteLine(id);
            }
        }


        //30. Write code to check if ProductID 789 exists.
        //todo: if correct result is bool on question 30

        public static void contains789()
        {
            var products = DataLoader.LoadProducts();



            var result = products.Select(i => i.ProductID).Contains(789);

            Console.WriteLine(result);
        }



        //31. Get a list of categories that have at least one product out of stock.

        public static void outofStockCat()
        {
            var products = DataLoader.LoadProducts();

            var result = (from p in products
                where p.UnitsInStock == 0
                select p.Category).Distinct();

            foreach (var category in result)
            {
                Console.WriteLine(category);
            }

        }


        //32. Determine if NumbersB contains only numbers less than 9.
        //todo: see if right return

        public static void lessThan9()
        {
            var numbersB = DataLoader.NumbersB;

            var result = numbersB.Max(n => n) < 9;


        }

        // 33. Get a grouped a list of products only for categories that have all of their products in stock.

        public static void CatWithPinStock()
        {

            var products = DataLoader.LoadProducts();


            var result = products.GroupBy(p => p.Category).Where(x => x.All(u => u.UnitsInStock > 0));

            foreach (var cat in result)
            {
                Console.WriteLine("\t \t {0}", cat.Key);
                foreach (var p in cat)
                {
                    Console.WriteLine("{0} with units {1}", p.ProductName, p.UnitsInStock);
                }
            }

        }


        //  34. Count the number of odds in NumbersA.

        public static void onlyOdds()
        {
            var numbersA = DataLoader.NumbersA;

            var result = numbersA.Count(n => n%2 != 0);

            Console.WriteLine("odd numbers is {0}", result);


        }

        // 35. Display a list of CustomerIDs and only the count of their orders.

        public static void customerIDCount()
        {
            var customers = DataLoader.LoadCustomers();

            var result = from c in customers
                select new
                {
                    c.CustomerID,
                    countOfOrder = c.Orders.Count()
                };

            foreach (var id in result)
            {
                Console.WriteLine("{0} - {1}", id.CustomerID, id.countOfOrder);
            }

        }

        // 36. Display a list of categories and the count of their products. todo: review

        public static void productCount()
        {
            var products = DataLoader.LoadProducts();

            //var result = from p in products
            //    group p.ProductName.Count() by p.Category
            //    into newT
            //             select newT;

            //foreach (var n in result)
            //{
            //   Console.WriteLine(n.Key);
            //    foreach (var count in n)
            //    {
            //        Console.WriteLine("{0}", count);
            //    }
            var result = from p in products
                group p by p.Category
                into catnew
                select new {catnew.Key, countProducts = catnew.Count()};

            foreach (var item in result)
            {
                Console.WriteLine("Category - {0}    Product Count - {1}", item.Key, item.countProducts );
            }




        }

            

                // 37. Display the total units in stock for each category.

            public static void totalUnitbyCat ()
            {
                var products = DataLoader.LoadProducts();

                var result = from p in products
                    group p by p.Category
                    into c
                    select new {c.Key, total = c.Sum(n => n.UnitsInStock)};

                foreach (var item in result)
                {
                    Console.WriteLine(" Category -{0}, Total in Stock-{1}", item.Key, item.total);

                }


        }


      //  38. Display the lowest priced product in each category.

        public static void lowestP()
        {
            var products = DataLoader.LoadProducts();

            var result = from p in products
                group p by p.Category
                into low
                select new {low.Key, lowest = low.Min(n => n.UnitPrice)};


            foreach (var item in result)
            {
                Console.WriteLine("Category- {0}    Price - {1}", item.Key, item.lowest);
            }

        }


      //  39. Display the highest priced product in each category.

        public static void highestP()
        {
            var products = DataLoader.LoadProducts();

            var result = from p in products
                group p by p.Category
                into H
                select new {H.Key, Highest = H.Max(n => n.UnitPrice)};

            foreach (var item in result)
            {
                Console.WriteLine("Category- {0}    Higest Price- {1}", item.Key, item.Highest);
            }




        }




        //40. Show the average price of product for each category.

        public static void averageP()
        {
            var products = DataLoader.LoadProducts();

            var result = from p in products
                group p by p.Category
                into A
                select new {A.Key, Average = A.Average(n => n.UnitPrice)};

            foreach (var item in result)
            {
                Console.WriteLine("Category- {0}    Average Price- {1}", item.Key, item.Average);
            }


        }
    }
}









